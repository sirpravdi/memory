class Memoji {
  constructor() {
    this.prev = null;
    this.roundTime = 60;

    this.form = document.querySelector('main');
    this.form.addEventListener('click', click => this.move(click.target), true);

    this.clock = document.querySelector('.timer');
    this.timer = new Timer(this.clock, this.roundTime);

    this.blockScreen = document.querySelector('.block');
    this.alertWindow = this.blockScreen.querySelector('.alert');
    this.alertStatus = this.blockScreen.querySelector('.status');
    this.restartButton = this.alertWindow.querySelector('.replay');

    this.restartButton.addEventListener('click', click => this.restart());

    this.icons = Array.from(document.querySelectorAll('.card'));

    this.shuffle();
  }

  sort() {
    return ['0x1F43C', '0x1F984', '0x1F983', '0x1F41F', '0x1F43C', '0x1F438', '0x1F41F', '0x1F984', '0x1F40A', '0x1F438', '0x1F983', '0x1F40A'].sort(() => Math.random() - 0.5);
  }

  shuffle() {
    this.cards = [];
    this.emojis = this.sort();
    this.emojis.forEach((item, i) => this.cards.push(this.assign(item, this.icons[i])));
  }

  assign(emoji, icon) {
    return new Card(icon, emoji);
  }

  move(target) {
    let filtered = this.cards.filter((item) => (item.element == target.parentNode));

    if (filtered == '') { return; }

    let clicked = filtered[0].element;
    let card = filtered[0].element.children[1];

    if ((card.classList.contains('card_match')) || (card.classList.contains('card_miss'))) {
      return;
    }

    if (!this.prev) {
      if (!this.timer.timerInit) {
        this.timer.setTimer();
      }

      clicked.classList.add('card_clicked');
      this.prev = filtered[0];
      return;
    }

    if (this.prev == filtered[0]) {
      return;
    }

    let wrong = Array.from(document.querySelectorAll('.card_miss'));

    if (wrong != '') {
      wrong.forEach(item => this.clear(item));
      clicked.classList.add('card_clicked');
      this.prev = filtered[0];
      return;
    }

    clicked.classList.add('card_clicked');
    this.check(this.prev, filtered[0]);

  }

  clear(card) {
    card.parentNode.classList.remove('card_clicked');
    card.classList.remove('card_miss');
    card.classList.remove('card_match');
  }

  check(card1, card2) {
    if (card1.emoji == card2.emoji) {
      card1.correct();
      card2.correct();
      this.prev = null;
      if (document.querySelectorAll('.card_match').length == 12) {
        this.timer.stop();
      }
    } else {
      card1.wrong();
      card2.wrong();
      this.prev = card2;
    }
  }

  lose() {
    this.blockScreen.style.display = 'inline-flex';
    this.alertWindow.style.display = 'inline-flex';

    this.alertStatus.innerHTML = '<span>L</span><span>o</span><span>s</span><span>e</span>';
    this.restartButton.textContent = 'Try again';
  }

  win() {
    this.blockScreen.style.display = 'inline-flex';
    this.alertWindow.style.display = 'inline-flex';

    this.alertStatus.innerHTML = '<span>W</span><span>i</span><span>n</span>';
    this.restartButton.textContent = 'Play again';
  }

  restart() {
    this.blockScreen.style.display = 'none';
    this.alertWindow.style.display = 'none';

    this.cards.forEach(item => this.clear(item.element.children[1]));

    this.shuffle();

    this.timer.roundTime = this.roundTime;
    this.timer.setClock();
  }
}

class Card {
  constructor(element, emoji) {
    this.element = element;
    this.emoji = emoji;
    this.element.children[1].textContent = String.fromCodePoint(this.emoji);
  }

  correct() {
    this.element.children[1].classList.add('card_match');
  }

  wrong() {
    this.element.children[1].classList.add('card_miss');
  }
}

class Timer {
  constructor(clock, roundTime) {
    this.clock = clock;
    this.roundTime = roundTime;
    this.setClock();
  }

  setTimer() {
    this.timerInit = setInterval(() => this.tick(), 1000);
    this.timeOut = setTimeout(() => this.stop(), this.roundTime * 1000 + 500);
  }

  setClock() {
    let minutes = Math.floor(this.roundTime / 60);
    let seconds = (this.roundTime % 60).toString();

    minutes = minutes.length == 2 ? minutes : '0' + minutes;
    seconds = seconds.length == 2 ? seconds : '0' + seconds;

    this.clock.textContent = minutes + ':' + seconds;
  }

  tick() {
    this.roundTime--;
    this.setClock();
  }

  stop() {
    clearInterval(this.timerInit);
    this.timerInit = null;
    game.prev = null;
    if (this.roundTime != 0) {
      clearTimeout(this.timeOut);
      setTimeout(() => game.win(), 1000);
      return;
    }
    game.lose();
  }
}

const game = new Memoji();